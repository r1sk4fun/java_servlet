import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.MouseInfo;


public class MousePositionServlet extends HttpServlet {
  
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    
    int x = (int) MouseInfo.getPointerInfo().getLocation().x;
    int y = (int) MouseInfo.getPointerInfo().getLocation().y;
    
    System.out.print(y);
    System.out.print(x);
    
    response.setContentType("text/html");
    response.getWriter().write("Mouse Position: x = " + x + ", y = " + y);
  }
}
